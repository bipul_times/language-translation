module.exports = (app) => {
    const translation = require('../controllers/translation.controller.js');

    // Create a new Note
    app.post('/translation/create', translation.create);
    app.post('/translation/update', translation.update);
    app.post('/translation/delete', translation.delete);
    app.get('/translation/search', translation.search);
    app.get('/all', translation.findAll);

    
}
//http://localhost:3000/translation/update