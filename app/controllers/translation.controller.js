const Translation = require('../models/translation.model.js');

// Create and Save a new Note
exports.create = (req, res) => {
    // Validate request
    // if(!req.body.content) {
    //     return res.status(400).send({
    //         message: "Note content can not be empty"
    //     });
    // }
    
    let request = req.body;
    // {
    //     "original": "pneer",
    //     "template": "all",
    //     "property": "nbt",
    //     "contents": [{
    //         "platform": "ios",
    //         "version": "608",
    //         "translation": "Apple2"
    //     }, {
    //         "platform": "ios",
    //         "version": "605",
    //         "translation": "Apple2"
    //     }]
    // };
    let prepareBulkInsert = [];
    request.contents.forEach((content, index) => {
        prepareBulkInsert[index] = {};
        prepareBulkInsert[index].original = request.original.toUpperCase();
        prepareBulkInsert[index].template = request.template.toUpperCase();
        prepareBulkInsert[index].property = request.property.toUpperCase();

        prepareBulkInsert[index].platform = content.platform.toUpperCase();
        prepareBulkInsert[index].version = content.version.toUpperCase();
        prepareBulkInsert[index].translation = content.translation;
        
    });
    

    Translation.saveTranslation(prepareBulkInsert).then(function(translated){       
        let response = Translation.prepareResponse(translated);       
        res.send(response);
    }).catch(err => {
        if(err.code == '11000'){
            var message = " Oops! Dupicate record found." 

        }else{
            var message = err || "Some error occurred while creating the Translation."
        }
        res.status(500).send({
            status: 'ERROR',
            message: message,
            raw: err.writeErrors
        });
    });
     
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Translation.find()
    .then(trans => {
        res.send(trans);
    }).catch(err => {
        res.status(500).send({
            status: 'ERROR',
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


// Find a single note with a noteId
exports.search = (req, res) => {
   
    var searchObject = {};
    if(req.query.t){
        searchObject.original = req.query.t.toUpperCase();
    }
    if(req.query.ppt){
        searchObject.property = req.query.ppt.toUpperCase();
    }
    if(req.query.tn){
        searchObject.template = req.query.tn.toUpperCase();
    }
    
    Translation.find(        
        searchObject
    )
    .then(trans => {
        res.send(trans);
    }).catch(err => {
        res.status(500).send({
            status: 'ERROR',
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


// Find a single note with a noteId
exports.findOne = (req, res) => {

};

// Update a note identified by the noteId in the request
exports.update = async (req, res) => {
    // Validate Request

    // if no id than check the matching paramaters
    // if id then simply update
    // if(!req.body.id){
    //     return res.status(400).send({
    //         status: 'ERROR',
    //         message: "Id is missing, its required to update"
    //     });
    // }
    
        // if(!req.body.content || !req.body.property ||  !req.body.template || !req.body.text_original) {
        //     return res.status(400).send({
        //         message: "Mandatory can not be empty"
        //     });
        // }
      
    var transObj = {                    
        content: req.body.content
    };
    let request = req.body;
    request = {
        "property": "11212111W11311112",
        "template": "ALL",
        "original": "PNEER",
        "contents": [{
                "id": "5ff84a410e60b67012fd9bb4",
                "platform": "IOS",
                "version": "606",
                "translation": "Apple4" 
            },
            {
                "id": "5ff84a410e60b67012fd9bb5",
                "platform": "IOS",
                "version": "605w",
                "translation": "Apple2"
            },
            {
               
                "platform": "IOS",
                "version": "6012",
                "translation": "Apple2"
            },
            {
               
                "platform": "IOS",
                "version": "600",
                "translation": "Apple2"
            }
        ]    
    };

        
        // Translation.deleteMany({_id:prepareForBulkDelete}, opts, function(err) {
        //     res.send(err);
        // });
        //this.deleteMany({_id:prepareForBulkDelete});
       Translation.updateRequest(request).then(translated=>{
           // let response = Translation.prepareResponse(translated);       
            res.send(translated);
       }).catch(err => {
        
            if(err.code == '11000'){
                var message = " Oops! Dupicate record found." 

            }else{
                var message = err || "Some error occurred while creating the Translation."
            }
            res.status(500).send({
                status: 'ERROR',
                message: message,
                raw: err.writeErrors
            });
        });
       
       
    //original,property,template,platform,version

    //let deleteCondition = { userUID: uid };

    // Translation.deleteIdsIN(["5ff7237d323da56f097f3264"]).then(translated => {
    //     if(!translated) {
    //         return res.status(404).send({
    //             status : 'ERROR',
    //             message: "Note not found with id " + req.body.id
    //         });
    //     }
    //     var response = {};
    //     response.status = 'OK';
    //     response.data = translated;
    //     res.send(response);
    // }).catch(err => {
        
    //     console.log(err);
    //     if(err.kind === 'ObjectId') {
    //         return res.status(404).send({
    //             status : 'ERROR',
    //             message: "Note not found with id " + req.body.id
    //         });                
    //     }
    //     return res.status(500).send({
    //         status : 'ERROR',
    //         message: "Error updating note with id " + req.body.id
    //     });
    // });

     

    
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => { 
    Translation.findByIdAndRemove(req.body.id)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                status: 'ERROR',
                message: "Translation not found with " + req.body.id
            });
        }
        res.send({
            status: 'OK',
            message: "Translation deleted successfully!"
        });
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                status: 'ERROR',
                message: "Translation not found with id " + req.body.id
            });                
        }
        return res.status(500).send({
            status: 'ERROR',
            message: "Could not delete translation with id " + req.body.id
        });
    });
};



// {
//     "original": "pneer",
//     "template": "all",
//     "property": "nbt",
//     "contents": [{
//         "platform": "ios",
//         "version": "608",
//         "translation": "Apple2"
//     }, {
//         "platform": "ios",
//         "version": "605",
//         "translation": "Apple2"
//     }]
// };