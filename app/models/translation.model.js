const mongoose = require('mongoose');

// var autoIncrement = require('mongoose-auto-increment');
// autoIncrement.initialize(mongoose.connection);

// var Schema = mongoose.Schema;
// var SchemaTypes = mongoose.Schema.Types;

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);



const TranslationSchema = mongoose.Schema({
    
    original: {
        type: String,
        required: true    
      },
    property:  {
        type: String,
        required: true    
      },
    template:  {
        type: String,
        required: true    
      },
    platform:  {
        type: String,
        required: true
    },
    version:  {
        type: String,
        required: true    
    },
    translation:  {
        type: String,
        required: true   
      },
}, {
    timestamps: true
});

// TranslationSchema.plugin(autoIncrement.plugin, {
//     model: 'translation',
//     field: '_id',
//     startAt: 1, // this may need to change with live last inset id
//     incrementBy: 1,
//     type: SchemaTypes.Long,
//     id:"_id"  
  
//   });

TranslationSchema.index({ original: 1, property: 1, template:1, platform:1,version:1}, { unique: true  });

TranslationSchema.statics.saveTranslation = function(transObj){ 
    // TO Add Logger
    return this.insertMany(transObj);  
};

// TranslationSchema.statics.deleteIdsIN = function(ids){ 
//     // TO Add Logger
//     return this.deleteMany({ _id:  ids});
// };

TranslationSchema.statics.updateTranslation = function(id, transObj){  
    
    return this.findByIdAndUpdate(id, transObj, function (err, translated) { 
        
    });
};

TranslationSchema.statics.prepareResponse = function(transObj){  
    let prepareBulkInsert = {};
    prepareBulkInsert['contents'] = [];
    transObj.forEach((content, index) => {
        prepareBulkInsert.property = content.property;
        prepareBulkInsert.template = content.template;
        prepareBulkInsert.original = content.original;
        
        prepareBulkInsert["contents"][index] = {};
        prepareBulkInsert["contents"][index].id = content._id;
        prepareBulkInsert["contents"][index].platform = content.platform;
        prepareBulkInsert["contents"][index].version = content.version;
        prepareBulkInsert["contents"][index].translation = content.translation;        
    });
    let response = {};    
    response.status =  'OK'; 
    response.data = prepareBulkInsert;
    return response;
};

// TranslationSchema.static.updateRequest = function(prepareForBulkDelete){  
//     console.log('ad');
//     return prepareForBulkDelete;
// };

TranslationSchema.statics.updateRequest = function(request){  
   
    
        // let prepareForBulkDelete = [];
        // request.remove.forEach((content, index) => {           
        //     prepareForBulkDelete.push(content.id);
        // });
        // var deletion =  this.deleteMany({_id:prepareForBulkDelete}).then(del=>{
        //     return del;
        // });

        
        let prepareBulkInsert = [];
        // request.contents.forEach((content, index) => {
        //     prepareBulkInsert[index] = {};
        //     prepareBulkInsert[index].original = request.original.toUpperCase();
        //     prepareBulkInsert[index].template = request.template.toUpperCase();
        //     prepareBulkInsert[index].property = request.property.toUpperCase();

        //     prepareBulkInsert[index].platform = content.platform.toUpperCase();
        //     prepareBulkInsert[index].version = content.version.toUpperCase();
        //     prepareBulkInsert[index].translation = content.translation;
        //     prepareBulkInsert[index]._id = content.id;

            
            
        // });

        request.contents.forEach((content, index) => {                
            if(content.id!=undefined){
                prepareBulkInsert.push({                    
                    updateOne: {
                        filter: { _id: content.id},
                        update: {
                           original:request.original.toUpperCase(),
                           template:request.template.toUpperCase(),
                           property:request.property.toUpperCase(),
    
                           platform:content.platform.toUpperCase(),
                           version:content.version.toUpperCase(),
                           translation:content.translation
                        }
                    }                   
                });
            }else{
                prepareBulkInsert.push({  
                    insertOne:{
                        document:{                        
                        original:request.original.toUpperCase(),
                        template:request.template.toUpperCase(),
                        property:request.property.toUpperCase(),
 
                        platform:content.platform.toUpperCase(),
                        version:content.version.toUpperCase(),
                        translation:content.translation
                     } 
                    }
                });
            }
        });
        console.log('prepareBulkInsert',prepareBulkInsert);
        var creation = this.bulkWrite(prepareBulkInsert);
    

        
            //var creation = this.saveTranslation(prepareBulkInsert);

            return creation;
};

module.exports = mongoose.model('Translation', TranslationSchema);

 

// upsert, unique

// const session = await this.startSession();
//     session.startTransaction();
//     try {
//         const opts = { session };
//         console.log(prepareForBulkDelete);
//         const A = await this.deleteMany({_id:prepareForBulkDelete}, opts, function(err) {
//         });
//         await session.commitTransaction();
//         session.endSession();
//         return true;
//     } catch (error) {
//         // If an error occurred, abort the whole transaction and
//         // undo any changes that might have happened
//         await session.abortTransaction();
//         session.endSession();
//         throw error; 
//     }
//     return 'OK';